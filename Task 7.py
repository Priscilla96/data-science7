#!/usr/bin/env python
# coding: utf-8

# In[202]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
from sklearn.cluster import KMeans


# In[203]:


data = pd.read_csv('Temperature-change-annual.csv')


# In[204]:


data


# In[205]:


x = data.iloc[:,[1,2]]


# x

# In[206]:


x


# In[207]:


plt.scatter(data['Warmest year ranking'],data['Temperature departure'])
plt.xlim(0,80)
plt.ylim(-3,6)
plt.show


# In[208]:


kmeans = KMeans(3)


# In[209]:


kmeans.fit(x)


# In[210]:


identified_clusters = kmeans.fit_predict(x)
identified_clusters


# In[211]:


data_with_clusters = data.copy()
data_with_clusters['Cluster'] = identified_clusters
data_with_clusters


# In[212]:


plt.scatter(data_with_clusters['Warmest year ranking'],data_with_clusters['Temperature departure'],c=data_with_clusters['Cluster'],cmap='rainbow')
plt.xlim(0,80)
plt.ylim(-3,6)
plt.show()

